This module adds a drag and drop element to webform components. It's
essentially an extension of the default webform file component.

Requirements

jQuery_update (jQuery >= 1.7)
webform

Quick Start

1. Enable the module.
2. Configure permissions for webform dragndrop
3. Add the 'Drag and drop upload element' component to any webform
4. Configure the element.

Note from the Developer

As IE doesn't support this method of dragging files into an input type file,
the element isn't displayed for these users, instead replaced with a default
file input.
